package com.adoyen_gmeheust.tregorrando.hike_details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.adoyen_gmeheust.tregorrando.R
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Polyline
import java.util.ArrayList

class HikeDetailsActivity : AppCompatActivity()
{
    private lateinit var viewModel: HikeDetailsViewModel

    private lateinit var hikeNom: TextView
    private lateinit var hikeID: TextView
    private lateinit var hikeLongueur: TextView
    private lateinit var hikeLieu: TextView
    private lateinit var hikeBalisage: TextView
    private lateinit var hikeVocation: TextView
    private lateinit var map: MapView

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Configuration.getInstance().load(
            applicationContext,
            PreferenceManager.getDefaultSharedPreferences(applicationContext)
        )
        setContentView(R.layout.hike_details_layout)
        viewModel = ViewModelProvider(this)[HikeDetailsViewModel::class.java]

        hikeNom = findViewById(R.id.hike_details_layout_hike_name)
        hikeNom.text = viewModel.getNom()

        hikeID = findViewById(R.id.hike_details_layout_hike_id)
        hikeID.text = viewModel.getId().toString()

        hikeLongueur = findViewById(R.id.hike_details_layout_hike_longueur)
        hikeLongueur.text = viewModel.getLongueur().toString()

        hikeLieu = findViewById(R.id.hike_details_layout_hike_lieu)
        hikeLieu.text = viewModel.getLieu()

        hikeBalisage = findViewById(R.id.hike_details_layout_hike_balisage)
        hikeBalisage.text = viewModel.getBalisage()

        hikeVocation = findViewById(R.id.hike_details_layout_hike_vocation)
        hikeVocation.text = viewModel.getVocation().toString()

        map = findViewById(R.id.map)
        map.setTileSource(TileSourceFactory.MAPNIK)
        map.setMultiTouchControls(true)
        val mapController = map.controller
        mapController.setZoom(18.0)

        if (viewModel.thereIsGeometry())
        {
            if (viewModel.getGeometryType() == "MultiLineString")
            {
                val coordinates = viewModel.getCoordinates() as List<List<List<Double>>>
                mapController.setCenter(GeoPoint(coordinates[0][0][1], coordinates[0][0][0]))

                for (coordinateList in coordinates)
                {
                    val points = ArrayList<GeoPoint>()
                    for (coordinate in coordinateList)
                    {
                        points.add(GeoPoint(coordinate[1], coordinate[0]))
                    }
                    val polyline = Polyline()
                    polyline.setPoints(points)
                    polyline.color = ContextCompat.getColor(this, R.color.red_tregor_rando)
                    map.overlayManager.add(polyline)
                }
            } else if (viewModel.getGeometryType() == "LineString")
            {
                val coordinates = viewModel.getCoordinates() as List<List<Double>>
                val points = ArrayList<GeoPoint>()
                mapController.setCenter(GeoPoint(coordinates[0][1], coordinates[0][0]))

                for (coordinate in coordinates)
                {
                    points.add(GeoPoint(coordinate[1], coordinate[0]))
                }

                val polyline = Polyline()
                polyline.setPoints(points)
                polyline.color = ContextCompat.getColor(this, R.color.red_tregor_rando)
                map.overlayManager.add(polyline)
            }
        }
        else
        {
            // Don't show the map as there is no geometry.
            val parent: ViewGroup = map.parent as ViewGroup
            parent.removeView(map)
        }
    }

    override fun onResume()
    {
        super.onResume()
        map.onResume()
    }

    override fun onPause()
    {
        super.onPause()
        map.onPause()
    }
}

