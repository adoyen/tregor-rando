package com.adoyen_gmeheust.tregorrando.hike_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adoyen_gmeheust.tregorrando.data.Feature
import com.adoyen_gmeheust.tregorrando.data.HikeType
import com.adoyen_gmeheust.tregorrando.search.SearchResultsSingleton

class HikeDetailsViewModel : ViewModel()
{
    private val _hike = MutableLiveData(SearchResultsSingleton.getHikes()[SearchResultsSingleton.getSelectedHikeIndex()])
    val hike : LiveData<Feature> = _hike

    fun getNom() : String
    {
        return this.hike.value!!.properties.nom
    }

    fun getId() : Int
    {
        return this.hike.value!!.properties.id
    }

    fun getLongueur(): Double
    {
        return this.hike.value!!.properties.longueur
    }

    fun getLieu(): String
    {
        return this.hike.value!!.properties.lieu
    }

    fun getBalisage(): String?
    {
        return this.hike.value!!.properties.balisage
    }

    fun getVocation(): HikeType?
    {
        return this.hike.value!!.properties.vocation
    }

    fun thereIsGeometry(): Boolean
    {
        return this.hike.value!!.geometry != null
    }

    fun getGeometryType(): String
    {
        return this.hike.value!!.geometry!!.type
    }

    fun getCoordinates():  List<List<Any>>
    {
        return this.hike.value!!.geometry!!.coordinates
    }
}