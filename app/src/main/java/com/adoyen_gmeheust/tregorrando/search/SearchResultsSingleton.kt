package com.adoyen_gmeheust.tregorrando.search

import com.adoyen_gmeheust.tregorrando.data.Feature

object SearchResultsSingleton
{
    private lateinit var hikes : List<Feature>
    private var selectedHikeIndex = 0

    fun setHikes(hikes : List<Feature>)
    {
        this.hikes = hikes
    }

    fun getHikes() : List<Feature>
    {
        return this.hikes
    }

    fun setSelectedHikeIndex(hikeIndex: Int)
    {
        this.selectedHikeIndex = hikeIndex
    }

    fun getSelectedHikeIndex(): Int
    {
        return selectedHikeIndex
    }
}