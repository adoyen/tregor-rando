package com.adoyen_gmeheust.tregorrando.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adoyen_gmeheust.tregorrando.data.Feature
import com.adoyen_gmeheust.tregorrando.data.HikeData
import com.adoyen_gmeheust.tregorrando.data.HikeType
import com.adoyen_gmeheust.tregorrando.network.HikeAPI
import kotlinx.coroutines.launch

class SearchViewModel : ViewModel()
{
    private val _hikes = MutableLiveData<HikeData>()
    val hikes: LiveData<HikeData> = _hikes

    fun getHikes()
    {
        viewModelScope.launch {
            try
            {
                _hikes.value = HikeAPI.retrofitService.getHikes()
            }
            catch (e : Exception)
            {
                _hikes.value = HikeData(listOf(), "")
                e.printStackTrace()
            }
        }
    }

    fun getTargetedHikes(hikeName : String, targetedHikeType : HikeType) : List<Feature>
    {
        val result = mutableListOf<Feature>()
        val filteredResultAsList : List<Feature>

        for (feature: Feature in hikes.value!!.features)
        {
            if (feature.properties.vocation == targetedHikeType)
            {
                result.add(feature)
            }
        }

        filteredResultAsList = result.filter {
            it.properties.nom.contains(hikeName, true)
        }

        return filteredResultAsList
    }
}