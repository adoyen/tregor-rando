package com.adoyen_gmeheust.tregorrando.search

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.adoyen_gmeheust.tregorrando.R
import com.adoyen_gmeheust.tregorrando.data.Feature
import com.adoyen_gmeheust.tregorrando.data.HikeData
import com.adoyen_gmeheust.tregorrando.data.HikeType
import com.adoyen_gmeheust.tregorrando.hike_details.HikeDetailsActivity
import com.adoyen_gmeheust.tregorrando.hike_list.HikeListActivity

class SearchActivity : AppCompatActivity()
{
    private lateinit var viewModel : SearchViewModel
    private lateinit var hikeNameEditText : EditText
    private lateinit var hikeTypeSpinner : Spinner
    private lateinit var loadingAnimation : ProgressBar

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_layout)
        viewModel = ViewModelProvider(this)[SearchViewModel::class.java]

        val hikesObserver = Observer<HikeData> { onSearchResult() }
        viewModel.hikes.observe(this, hikesObserver)

        this.hikeTypeSpinner = findViewById(R.id.search_layout_type_of_hike_spinner)

        ArrayAdapter.createFromResource(this, R.array.hike_types, android.R.layout.simple_spinner_item).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            this.hikeTypeSpinner.adapter = adapter
        }

        this.loadingAnimation = findViewById(R.id.search_layout_progress_bar)

        val searchButton = findViewById<Button>(R.id.search_layout_search_button)
        searchButton.setOnClickListener {
            this.loadingAnimation.visibility = View.VISIBLE
            viewModel.getHikes()
        }

        this.hikeNameEditText = findViewById(R.id.search_layout_search_bar_hike_name)
    }

    private fun onSearchResult()
    {
        val selectedHikeName = this.hikeNameEditText.text.toString()
        val selectedHikeType = HikeType.fromString(this.hikeTypeSpinner.selectedItem.toString())
        val hikes : List<Feature>
        val intent : Intent

        hikes = this.viewModel.getTargetedHikes(selectedHikeName, selectedHikeType)
        SearchResultsSingleton.setHikes(hikes)
        this.loadingAnimation.visibility = View.INVISIBLE

        if (hikes.isEmpty())
        {
            Toast.makeText(this, "Aucune randonnée trouvée selon ces critères", Toast.LENGTH_SHORT).show()
        }
        else if (hikes.size == 1)
        {
            SearchResultsSingleton.setSelectedHikeIndex(0)
            intent = Intent(this, HikeDetailsActivity::class.java)
            this.startActivity(intent)
        }
        else
        {
            intent = Intent(this, HikeListActivity::class.java)
            this.startActivity(intent)
        }
    }
}