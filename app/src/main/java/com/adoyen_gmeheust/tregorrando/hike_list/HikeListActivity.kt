package com.adoyen_gmeheust.tregorrando.hike_list

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adoyen_gmeheust.tregorrando.R

class HikeListActivity : AppCompatActivity()
{
    private lateinit var recyclerViewList : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hike_list_layout)

        recyclerViewList = findViewById(R.id.hike_list_layout_hikes_recycler_view)
        recyclerViewList.layoutManager = LinearLayoutManager(this)
        recyclerViewList.adapter = HikeListAdapter()
    }
}