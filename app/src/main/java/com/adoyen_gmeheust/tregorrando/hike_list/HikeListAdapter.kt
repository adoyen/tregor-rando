package com.adoyen_gmeheust.tregorrando.hike_list

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.adoyen_gmeheust.tregorrando.R
import com.adoyen_gmeheust.tregorrando.hike_details.HikeDetailsActivity
import com.adoyen_gmeheust.tregorrando.search.SearchResultsSingleton

class HikeListAdapter : RecyclerView.Adapter<HikeListAdapter.HikeListViewHolder>()
{
    private val hikes = SearchResultsSingleton.getHikes()

    class HikeListViewHolder(val view : View) : RecyclerView.ViewHolder(view)
    {
        val button: Button = view.findViewById(R.id.hike_list_layout_results_button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HikeListViewHolder
    {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.hike_list_layout_result_button, parent, false)

        return HikeListViewHolder(layout)
    }

    override fun onBindViewHolder(holder: HikeListViewHolder, position: Int)
    {
        holder.button.text = hikes[position].properties.nom

        holder.button.setOnClickListener {
            val context = holder.view.context
            SearchResultsSingleton.setSelectedHikeIndex(position)
            val intent = Intent(context, HikeDetailsActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int
    {
        return hikes.size
    }
}