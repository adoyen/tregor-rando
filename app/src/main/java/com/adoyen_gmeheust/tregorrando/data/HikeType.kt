package com.adoyen_gmeheust.tregorrando.data

import com.squareup.moshi.Json

enum class HikeType
{
    @Json(name = "Pédestre") PEDESTRE,
    @Json(name = "VTT") VTT,
    @Json(name = "Vélo") VELO,
    @Json(name = "Equestre") EQUESTRE;

    companion object
    {
        fun fromString(string : String) : HikeType
        {
            val result : HikeType = when (string) {
                "Pédestre" -> PEDESTRE
                "VTT" -> VTT
                "Vélo" -> VELO
                "Equestre" -> EQUESTRE
                else -> throw IllegalArgumentException()
            }

            return result
        }
    }
}