package com.adoyen_gmeheust.tregorrando.data

import com.squareup.moshi.Json
import java.io.Serializable

data class HikeData(
    val features: List<Feature>,
    val type: String
) : Serializable

data class Feature(
    val geometry: Geometry?,
    val properties: Properties,
    val type: String
) : Serializable

data class Geometry(
    val coordinates: List<List<Any>>,
    val type: String
) : Serializable

data class Properties(
    @Json(name = "id__") val idKey: String,
    @Json(name = "iti_id") val id: Int,
    @Json(name = "iti_com_li") val lieu: String,
    @Json(name = "iti_long") val longueur: Double,
    @Json(name = "iti_com_in") val insee: Int,
    @Json(name = "iti_balisa") val balisage: String?,
    @Json(name = "iti_sens_p") val sensParcours: String?,
    @Json(name = "iti_vocati") val vocation: HikeType?,
    @Json(name = "iti_nom") val nom: String
) : Serializable