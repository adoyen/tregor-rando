package com.adoyen_gmeheust.tregorrando.network

import com.adoyen_gmeheust.tregorrando.data.HikeData
import com.adoyen_gmeheust.tregorrando.network.UnsafeOkHttpClient.Companion.getUnsafeOkHttpClient
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

private const val BASE_URL = "https://datarmor.cotesdarmor.fr"

private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
private val retrofit = Retrofit.Builder().addConverterFactory(MoshiConverterFactory.create(moshi)).baseUrl(BASE_URL).client(getUnsafeOkHttpClient().build()).build()

interface HikeAPIService
{
    @GET("/dataserver/cg22/data/itineraires_randonnees_ltc?&\$format=geojson&\$top=500")
    suspend fun getHikes() : HikeData
}

object HikeAPI
{
    val retrofitService : HikeAPIService by lazy { retrofit.create(HikeAPIService::class.java) }
}